/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 19:29:21 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/31 18:56:32 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include "get_next_line.h"
# include <wchar.h>

# define MAGENTA "\033[35m"
# define BMAGENTA "\033[37m"
# define CYAN "\033[1;36m"
# define BCYAN "\033[46mCyan"
# define GREEN "\033[1;32;40m"
# define BGREEN "\033[1;32;43m"
# define YELLOW "\033[1;33;40m"
# define BYELLOW "\033[1;33;45m"
# define RED "\033[1;31;40m"
# define BRED "\033[1;31;43m"
# define BLUE "\033[1;34;40m"
# define BBLUE "\033[1;34;46m"
# define RESET "\033[0m"

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_listdbl
{
	void				*content;
	size_t				content_size;
	struct s_listdbl	*next;
	struct s_listdbl	*prev;
}					t_listdbl;

typedef struct		s_listaz
{
	t_listdbl	**alist;
	t_listdbl	**zlist;
}					t_listaz;

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
size_t				ft_strlen(const char *s);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strdup(const char *s1);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
int					ft_atoi(const char *str);
void				*ft_memchr(const void *s, int c, size_t n);
void				ft_putnbr(int n);
void				ft_putnbr_dl(int n);
void				ft_putnbr_fd(int n, int fd);
void				ft_putchar(char c);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr(char const *s);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl(char const *s);
void				ft_putendl_fd(char const *s, int fd);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);
char				*ft_itoa(int n);
int					ft_intlength(int n);
size_t				ft_strlentoc(const char *src, char c);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
t_list				*ft_lstnew(void const *content, size_t content_size);
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
void				ft_lstaddend(t_list **alst, t_list *new_elem);
t_listdbl			*ft_lstdblnew(void const *content, size_t content_size);
void				ft_lstdbldel(t_listdbl **alst, void (*del)(void *, size_t));
void				ft_lstdbldelone(t_listdbl **alst,
		void (*del)(void *, size_t));
void				ft_lstdbladd(t_listdbl **alst, t_listdbl **zlst,
		t_listdbl *new);
void				ft_lstdbladdend(t_listdbl **alst, t_listdbl **zlst,
		t_listdbl *new_elem);
void				ft_putnbr_ull(unsigned long long int n);
void				ft_putnbr_ll(long long int n);
int					ft_ll_len(long long int n);
void				ft_putstr_n(char *s, int start, int len);
char				*ft_strjoin_c(char c);
char				*ft_strjoin_null(char const *s1, char const *s2);
int					ft_ull_len(unsigned long long int n);
char				*ft_ulltoa(unsigned long long int arg);
int					ft_wchar_len(wchar_t arg);
void				ft_putstr_col(char *s, char *color);
long long int		ft_atoll(char *str);

#endif
