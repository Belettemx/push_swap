/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 14:58:25 by agauci-d          #+#    #+#             */
/*   Updated: 2015/04/01 21:38:27 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/includes/libft.h"
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>

typedef struct	s_ps
{
	int			nbr;
	int			pos;
	int			isempty;
	int			rp;
	struct s_ps	*next;
	struct s_ps	*prev;
}				t_ps;

typedef struct	s_env
{
	t_ps		*a_first;
	t_ps		*a_last;
	t_ps		*b_first;
	t_ps		*b_last;
	t_ps		*c_first;
	t_ps		*c_last;
	int			isc_empty;
	int			isa_empty;
	int			stop;
	int			b_pb_first;
	int			v;
	int			p;
	int			e;
	int			r;
	int			c;
	int			begin;
	int			ss;
	int			rr;
	int			rrr;
	int			diff;
	int			cnt;
	int			j;
	int			val;
	int			place;
}				t_env;

void			ft_error_gestion(char *s);
void			ft_ps_delone(t_ps **first);
void			ft_ps_add_end(t_ps **first, t_ps **last, t_ps *new_elem);
void			ft_ps_del(t_ps **first, void (*del)(int, int));
void			ft_ps_add(t_ps **first, t_ps **last, t_ps *new);
void			ft_ps_add_btw(t_ps **before, t_ps **after, t_ps *new);
int				ft_check_error(char *s);
void			ft_check_double(t_env *env, t_ps *tmp);
t_ps			*ft_ps_new(int nbr, int pos, int isempty);
void			ft_init_env(t_env *env);
int				ft_sa(t_env *env);
int				ft_sb(t_env *env);
int				ft_ss(t_env *env);
int				ft_pa(t_env *env);
int				ft_pb(t_env *env);
int				ft_ra(t_env *env);
int				ft_rb(t_env *env);
int				ft_rr(t_env *env);
int				ft_rra(t_env *env);
int				ft_rrb(t_env *env);
int				ft_rrr(t_env *env);
int				ft_check_al_sort(t_env *env);
int				ft_algorithme(t_env *env);
int				ft_simple_algo(t_env *env);
void			ft_add_rp(t_env *env);
int				ft_top_a_smallest_rp(t_env *env);
int				ft_lstlen(t_ps *first);
int				ft_r_or_rr(t_ps *first, int cmp, int rp);
void			ft_option(t_env *env, char c);
void			ft_print_list(t_ps *first, int i);
void			ft_operation(t_env *env, char *s);
int				ft_diff_nbr(t_env *env);
int				ft_two(t_env *env);
int				ft_biggest_rp(t_ps *first);
int				ft_mix_ab(t_env *env);
int				ft_short_list(t_env *env);
int				ft_check_sort_rest(t_ps *a);
int				ft_check_last_inverse(t_env *env);

#endif
