/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_r_or_rr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 13:51:30 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/31 22:06:31 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_r_or_rr(t_ps *first, int cmp, int rp)
{
	int i;
	int len;

	i = 0;
	len = ft_lstlen(first);
	if (cmp)
	{
		while (first->next != NULL && cmp < first->rp)
		{
			first = first->next;
			i++;
		}
	}
	else
	{
		while (first->next != NULL && first->rp != rp)
		{
			first = first->next;
			i++;
		}
	}
	if (i > len / 2)
		return (2);
	else
		return (1);
}
