/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdblnew.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:23:26 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdlib.h>
#include <limits.h>

t_ps *ft_ps_new(int nbr, int pos, int isempty)
{
	t_ps	*elem;

	elem = (t_ps *)malloc(sizeof(t_ps));
	if (nbr >= INT_MAX || !elem)
		return (NULL);
	if (elem)
	{
		if (isempty == 0)
		{
			elem->nbr = 0;
			elem->pos = pos;
			elem->rp = 0;
			elem->isempty = 0;
		}
		else
		{
			elem->nbr = nbr;
			elem->pos = pos;
			elem->rp = 0;
			elem->isempty = 1;
		}
		elem->next = NULL;
		elem->prev = NULL;
	}
	return (elem);
}
