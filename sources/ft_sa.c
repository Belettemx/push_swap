/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sa.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 21:45:51 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:24:01 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_sa(t_env *env)
{
	int		tmp;
	int		tmp2;

	tmp = 0;
	tmp2 = 0;
	if (env->a_first != NULL && env->a_first->next != NULL)
	{
		tmp = env->a_first->nbr;
		tmp2 = env->a_first->rp;
		env->a_first->nbr = env->a_first->next->nbr;
		env->a_first->rp = env->a_first->next->rp;
		env->a_first->next->nbr = tmp;
		env->a_first->next->rp = tmp2;
		if (!env->rr && !env->ss && !env->rrr)
			ft_operation(env, "sa");
		if (env->v)
			ft_option(env, 'v');
	}
	return (1);
}
