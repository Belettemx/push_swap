/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_option.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 16:19:58 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:22:07 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void ft_print_lst(t_env *env, int i)
{
	ft_putchar('\n');
	ft_putendl("***** pile a *****");
	ft_print_list(env->a_first, i);
	ft_putendl("***** pile b *****");
	ft_print_list(env->b_first, i);
	ft_putchar('\n');
}

void		ft_option(t_env *env, char c)
{
	if (c == 'e')
	{
		ft_print_lst(env, env->p);
	}
	else if (c == 'v')
		ft_print_lst(env, env->p);
}
