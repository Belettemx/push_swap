/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_short_list.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/31 19:44:36 by agauci-d          #+#    #+#             */
/*   Updated: 2015/04/01 21:37:25 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_check_begin_pile(t_env *env)
{
	t_ps		*lst;
	int			cnt;
	int			val;

	lst = env->a_first->next;
	cnt = 0;
	val = 0;
	while (lst->next != NULL)
	{
		if (lst->nbr < lst->next->nbr)
			val++;
		lst = lst->next;
		cnt++;
	}
	if (val == cnt && env->a_first->nbr > lst->nbr)
		return (1);
	return (0);
}

static int	ft_begin_sort(t_env *env)
{
	int i;

	i = 0;
	if (ft_check_begin_pile(env) == 1)
	{
		i += ft_ra(env);
		i += ft_begin_sort(env);
	}
	if (env->a_first->nbr > env->a_first->next->nbr)
	{
		i += ft_sa(env);
		i += ft_begin_sort(env);
	}
	if (ft_check_last_inverse(env) == 1)
	{
		i += ft_rra(env);
		i += ft_rra(env);
		i += ft_sa(env);
		i += ft_ra(env);
		i += ft_ra(env);
		i += ft_begin_sort(env);
	}
	return (i);
}

static int	ft_sort_pile3(t_env *env, int ret)
{
	int i;
	int cpt;

	i = 0;
	cpt = -1;
	if (ret == 2)
	{
		while (ft_check_sort_rest(env->a_first) != 1 && ++cpt < env->place)
			i += ft_rra(env);
	}
	else
	{
		while (ft_check_sort_rest(env->a_first) != 1 && ++cpt < env->place)
			i += ft_ra(env);
	}
	return (i);
}

static int	ft_sort_pile2(t_env *env)
{
	t_ps	*lst_a;
	int		i;
	int		ret;

	i = 0;
	lst_a = env->a_first;
	while (lst_a != NULL)
	{
		if (env->val > lst_a->nbr)
		{
			env->val = lst_a->nbr;
			env->place = env->cnt;
		}
		env->cnt++;
		lst_a = lst_a->next;
	}
	ret = ft_r_or_rr(env->a_first, 0, env->place);
	i += ft_sort_pile3(env, ret);
	return (i);
}

int			ft_short_list(t_env *env)
{
	int i;

	i = 0;
	env->j = 0;
	while (ft_check_sort_rest(env->a_first) != 1)
	{
		i += ft_begin_sort(env);
		env->cnt = 0;
		env->val = env->a_first->nbr;
		env->place = 0;
		i += ft_sort_pile2(env);
		if (ft_check_sort_rest(env->a_first) != 1)
		{
			i += ft_pb(env);
			env->j++;
		}
	}
	while (env->b_first)
		i += ft_pa(env);
	return (i);
}
