/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_al_sort.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/14 19:44:04 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/31 21:52:29 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_check_al_sort(t_env *env)
{
	int		i;
	t_ps	*a;
	t_ps	*c;

	i = 1;
	a = env->a_first;
	c = env->c_first;
	while (a && i < env->isa_empty)
	{
		if (a->pos != c->pos)
			return (0);
		a = a->next;
		c = c->next;
		i++;
	}
	if (!a)
		return (0);
	return (1);
}
