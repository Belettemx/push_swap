/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ss.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/10 17:47:37 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 17:06:59 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_ss(t_env *env)
{
	env->ss = 1;
	ft_sa(env);
	ft_sb(env);
	env->ss = 0;
	ft_operation(env, "ss");
	if (env->v)
		ft_option(env, 'v');
	return (1);
}
