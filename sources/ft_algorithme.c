/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_algorithme.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/12 13:39:18 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/31 20:58:36 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ft_algo_ter(t_ps *b, t_env *env)
{
	int i;
	int biggest_rp;

	i = 0;
	b = env->b_first;
	biggest_rp = ft_biggest_rp(env->b_first);
	while (b->rp != biggest_rp)
	{
		i += ft_rb(env);
		b = env->b_first;
	}
	while (b != NULL)
	{
		i += ft_pa(env);
		b = env->b_first;
	}
	return (i);
}

static int		ft_algo_bis(t_ps *a, t_ps *b, t_env *env)
{
	int i;

	i = 0;
	if (a && a->next && a->rp > a->next->rp)
		i += ft_sa(env);
	else if (b == NULL)
	{
		if (env->b_pb_first == 0)
			env->b_pb_first = a->rp;
		i += ft_pb(env);
	}
	else if (a->rp > b->rp)
		i += ft_pb(env);
	else if (a->rp < b->rp && a->rp != env->stop && ft_lstlen(env->a_first) > 3)
	{
		if (env->stop == 0)
			env->stop = a->rp;
		i += ft_ra(env);
	}
	else
		i += ft_mix_ab(env);
	return (i);
}

int				ft_algorithme(t_env *env)
{
	t_ps	*a;
	t_ps	*b;
	int		i;
	int		ret;

	i = 0;
	i += ft_top_a_smallest_rp(env);
	a = env->a_first;
	b = env->b_first;
	while ((ret = ft_check_al_sort(env)) != 1)
	{
		if (a)
		{
			i += ft_algo_bis(a, b, env);
			b = env->b_first;
			a = env->a_first;
		}
		else if (!a)
		{
			i += ft_algo_ter(b, env);
			return (i);
		}
	}
	return (i);
}
