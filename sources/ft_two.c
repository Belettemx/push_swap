/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_two.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/17 16:41:56 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:25:06 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_three(t_env *env)
{
	int i;

	i = 0;
	if (env->isa_empty == 3)
	{
		i += ft_ra(env);
		i += ft_sa(env);
	}
	return (i);
}

static int	ft_last_switch(t_env *env)
{
	int i;

	i = 0;
	i += ft_rra(env);
	i += ft_rra(env);
	i += ft_sa(env);
	i += ft_ra(env);
	i += ft_ra(env);
	return (i);
}

int			ft_two(t_env *env)
{
	int		i;
	t_ps	*a;
	t_ps	*c;

	a = env->a_first;
	c = env->c_first;
	i = 0;
	while (a->rp == c->rp && a->next != NULL && c->next != NULL)
	{
		a = a->next;
		c = c->next;
	}
	if (a->rp == 2)
		i += ft_sa(env);
	else if (a->rp == env->isa_empty && a->prev != NULL)
		i += ft_last_switch(env);
	else if (a->rp == env->isa_empty && a->prev == NULL)
		i += ft_three(env);
	else
		i += ft_algorithme(env);
	return (i);
}
