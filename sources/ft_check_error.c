/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 17:15:26 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/31 19:06:50 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "limits.h"

int				ft_check_error(char *s)
{
	long long int	tmp2;
	int				tmp;
	int				i;
	int				flag;

	i = -1;
	flag = 1;
	tmp2 = ft_atoll(s);
	if (tmp2 > INT_MAX || tmp2 < INT_MIN)
		ft_error_gestion("Error\n");
	else
		tmp = (int)tmp2;
	while (s[++i])
	{
		if (flag && (s[0] == '-' || s[0] == '+'))
		{
			flag = 0;
			i++;
		}
		if (!(ft_isdigit(s[i])) && s[i] != '\0')
			ft_error_gestion("Error\n");
	}
	return (tmp);
}
