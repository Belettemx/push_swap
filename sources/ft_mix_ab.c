/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mix_ab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/17 19:03:05 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 16:15:48 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_mix_ab_bis(t_ps *a, t_ps *b, t_env *env)
{
	int i;

	i = 0;
	while (a->rp < b->rp)
	{
		i += ft_rb(env);
		b = env->b_first;
	}
	if (a->rp > b->rp)
	{
		i += ft_pb(env);
		a = env->a_first;
		b = env->b_first;
	}
	return (i);
}

static int	ft_mix_ter(t_ps *a, t_env *env, int biggest_rp)
{
	int i;
	int tmp;

	i = 0;
	tmp = 0;
	while (a && a->rp != biggest_rp)
	{
		if (tmp == 0)
			tmp = ft_r_or_rr(env->a_first, 0, biggest_rp) == 1 ? 1 : 2;
		if (tmp == 1)
			i += ft_ra(env);
		else
			i += ft_rra(env);
		a = env->a_first;
	}
	return (i);
}

int			ft_mix_ab(t_env *env)
{
	int		i;
	t_ps	*a;
	t_ps	*b;
	int		biggest_rp;

	i = 0;
	a = env->a_first;
	b = env->b_first;
	biggest_rp = ft_biggest_rp(env->a_first);
	while (a)
	{
		if (a->rp == biggest_rp)
		{
			i += ft_mix_ab_bis(a, b, env);
			a = env->a_first;
			b = env->b_first;
			if (a)
				biggest_rp = ft_biggest_rp(env->a_first);
			a = env->a_first;
		}
		i += ft_mix_ter(a, env, biggest_rp);
		a = env->a_first;
	}
	return (i);
}
