/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rb.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 15:04:37 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 17:02:39 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_rb(t_env *env)
{
	t_ps	*tmp;

	if (env->b_first != NULL && env->b_first->next != NULL \
			&& env->b_first->next->next != NULL)
	{
		tmp = env->b_first;
		env->b_first = env->b_first->next;
		env->b_first->prev = NULL;
		tmp->next = NULL;
		tmp->prev = env->b_last;
		env->b_last->next = tmp;
		env->b_last = tmp;
		if (!env->rr)
			ft_operation(env, "rb");
		if (env->v)
			ft_option(env, 'v');
	}
	else
		ft_sb(env);
	return (1);
}
