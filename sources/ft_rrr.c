/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rrr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 16:47:37 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 17:05:01 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_rrr(t_env *env)
{
	env->rrr = 1;
	ft_rra(env);
	ft_rrb(env);
	env->rrr = 0;
	ft_operation(env, "rrr");
	if (env->v)
		ft_option(env, 'v');
	return (1);
}
