/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_simple_algo.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/14 19:52:04 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 15:31:46 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_simple_algo(t_env *env)
{
	int i;
	int flag;

	i = 0;
	flag = 1;
	while (flag)
	{
		flag = 0;
		if (env->diff == 2)
		{
			i += ft_two(env);
			return (i);
		}
	}
	return (i);
}
