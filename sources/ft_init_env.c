/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 17:11:50 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:13:39 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void ft_init_env(t_env *env)
{
	env->a_first = NULL;
	env->a_last = NULL;
	env->b_first = NULL;
	env->b_last = NULL;
	env->c_first = NULL;
	env->c_last = NULL;
	env->isa_empty = 0;
	env->stop = 0;
	env->b_pb_first = 0;
	env->v = 0;
	env->p = 0;
	env->e = 0;
	env->r = 0;
	env->c = 0;
	env->begin = 1;
	env->diff = 0;
}
