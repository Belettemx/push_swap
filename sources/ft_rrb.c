/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rrb.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 15:47:36 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 17:04:42 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdio.h>

int ft_rrb(t_env *env)
{
	t_ps	*tmp;

	if (env->b_last != NULL && env->b_last->prev != NULL && \
			env->b_last->prev->prev != NULL)
	{
		tmp = env->b_last;
		tmp->next = env->b_first;
		env->b_first->prev = tmp;
		env->b_first = tmp;
		tmp = env->b_last->prev;
		env->b_first->prev = NULL;
		env->b_last = tmp;
		env->b_last->next = NULL;
		if (!env->rrr)
			ft_operation(env, "rrb");
		if (env->v)
			ft_option(env, 'v');
	}
	else
		ft_sb(env);
	return (1);
}
