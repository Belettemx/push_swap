/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_diff_nbr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/17 14:34:33 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 20:53:47 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_diff_nbr(t_env *env)
{
	int		i;
	int		b;
	t_ps	*a;
	t_ps	*c;

	i = 1;
	b = 0;
	a = env->a_first;
	c = env->c_first;
	while (a && i <= env->isa_empty)
	{
		if (a->pos != c->pos)
			b++;
		a = a->next;
		c = c->next;
		i++;
	}
	return (b);
}
