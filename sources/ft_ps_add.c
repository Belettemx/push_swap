/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdbladd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/15 21:38:00 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void ft_ps_add(t_ps **first, t_ps **last, t_ps *new)
{
	if (!first && !*first && !last && !*last)
	{
		ft_error_gestion("Error : ft_ps_add()");
		return ;
	}
	else if (*first == NULL)
	{
		*first = new;
		*last = new;
	}
	else if (first && *first && new)
	{
		new->next = *first;
		new->prev = NULL;
		(*first)->prev = new;
		*first = new;
	}
}
