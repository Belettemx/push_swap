/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_list.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 22:02:47 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/16 17:11:51 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_print(t_ps *cpy, int i)
{
	if (i)
	{
		ft_putstr("rp = ");
		ft_putnbr(cpy->rp);
		ft_putstr(" pos = ");
		ft_putnbr(cpy->pos);
	}
	if (i)
		ft_putstr(" nbr = ");
	else
		ft_putstr("nbr = ");
	ft_putnbr_dl(cpy->nbr);
}

void		ft_print_list(t_ps *first, int i)
{
	t_ps *cpy;

	if (!first)
		ft_putendl("empty list");
	else if (first)
	{
		cpy = first;
		if (cpy->next != NULL)
		{
			ft_print(cpy, i);
			ft_print_list(cpy->next, i);
		}
		else
			ft_print(cpy, i);
	}
}
