/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ps_add_btw.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/13 19:15:39 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:22:53 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void ft_ps_add_btw(t_ps **before, t_ps **after, t_ps *new)
{
	t_ps *tmp;

	if (!before && !*before && !after && !*after)
	{
		ft_error_gestion("Error: ft_ps_add_btw()");
		return ;
	}
	else
	{
		tmp = *before;
		new->prev = *before;
		new->next = *after;
		tmp->next = new;
		tmp = *after;
		tmp->prev = new;
	}
}
