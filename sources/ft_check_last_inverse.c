/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_last_inverse.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 21:31:37 by agauci-d          #+#    #+#             */
/*   Updated: 2015/04/01 21:33:25 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	ft_check_last_inverse(t_env *env)
{
	t_ps *lst;

	lst = env->a_first;
	while (lst->next->next != NULL)
		lst = lst->next;
	if (lst->next->nbr < lst->nbr && lst->next->nbr > env->a_first->nbr)
		return (1);
	return (0);
}
