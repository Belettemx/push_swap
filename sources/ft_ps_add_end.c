/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ps_add_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 21:36:41 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:23:06 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void ft_ps_add_end(t_ps **first, t_ps **last, t_ps *new_elem)
{
	t_ps	*cpy;

	if (!first && !*first)
	{
		ft_putstr("error ft_ps_add_end()");
		return ;
	}
	if (*first == NULL)
	{
		*first = new_elem;
		*last = new_elem;
	}
	else if (first && *first && new_elem)
	{
		cpy = *first;
		while (cpy->next != NULL)
			cpy = cpy->next;
		new_elem->prev = cpy;
		cpy->next = new_elem;
		*last = new_elem;
	}
}
