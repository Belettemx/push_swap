/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sb.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/10 16:37:56 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 17:06:46 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_sb(t_env *env)
{
	int		tmp;
	int		tmp2;

	tmp = 0;
	tmp2 = 0;
	if (env->b_first != NULL && env->b_first->next != NULL)
	{
		tmp = env->b_first->nbr;
		tmp2 = env->b_first->rp;
		env->b_first->nbr = env->b_first->next->nbr;
		env->b_first->rp = env->b_first->next->rp;
		env->b_first->next->nbr = tmp;
		env->b_first->next->rp = tmp2;
		if (!env->rr && !env->ss && !env->rrr)
			ft_operation(env, "sb");
		if (env->v)
			ft_option(env, 'v');
	}
	return (1);
}
