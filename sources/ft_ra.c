/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ra.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 15:04:37 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 17:07:41 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_ra(t_env *env)
{
	t_ps	*tmp;

	if (env->a_first != NULL && env->a_first->next != NULL && \
			env->a_first->next->next != NULL)
	{
		tmp = env->a_first;
		env->a_first = env->a_first->next;
		tmp->next = NULL;
		env->a_last->next = tmp;
		tmp->prev = env->a_last;
		env->a_last = tmp;
		if (!env->rr)
			ft_operation(env, "ra");
		if (env->v)
			ft_option(env, 'v');
	}
	else
		ft_sa(env);
	return (1);
}
