/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_rp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/15 17:40:07 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 15:28:53 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static t_ps		*ft_add_rp_bis(t_ps *a, t_ps *c, t_env *env)
{
	while (c->next != NULL)
	{
		if (c->nbr == a->nbr)
		{
			a->rp = c->rp;
			c = c->next;
		}
		if (a->next != NULL)
			a = a->next;
		else
			a = env->a_first;
	}
	return (c);
}

void			ft_add_rp(t_env *env)
{
	t_ps	*a;
	t_ps	*c;
	int		i;

	i = 1;
	c = env->c_first;
	while (c->next != NULL)
	{
		c->rp = i;
		c = c->next;
		i++;
	}
	c->rp = i;
	c = env->c_first;
	a = env->a_first;
	c = ft_add_rp_bis(a, c, env);
	a = env->a_first;
	while (a->nbr != c->nbr)
		a = a->next;
	a->rp = c->rp;
}
