/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_operation.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 17:35:43 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:21:45 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void ft_print_color(char *s)
{
	if (ft_strcmp(s, "sa") == 0)
		ft_putstr_col(s, MAGENTA);
	if (ft_strcmp(s, "sb") == 0)
		ft_putstr_col(s, BMAGENTA);
	if (ft_strcmp(s, "ss") == 0)
		ft_putstr_col(s, BBLUE);
	if (ft_strcmp(s, "pa") == 0)
		ft_putstr_col(s, CYAN);
	if (ft_strcmp(s, "pb") == 0)
		ft_putstr_col(s, BLUE);
	if (ft_strcmp(s, "ra") == 0)
		ft_putstr_col(s, GREEN);
	if (ft_strcmp(s, "rb") == 0)
		ft_putstr_col(s, BGREEN);
	if (ft_strcmp(s, "rr") == 0)
		ft_putstr_col(s, RED);
	if (ft_strcmp(s, "rra") == 0)
		ft_putstr_col(s, YELLOW);
	if (ft_strcmp(s, "rrb") == 0)
		ft_putstr_col(s, BYELLOW);
	if (ft_strcmp(s, "rrr") == 0)
		ft_putstr_col(s, BRED);
}

void		ft_operation(t_env *env, char *s)
{
	int i;

	i = env->begin ? 1 : 0;
	if (i)
		env->begin = 0;
	if (env->r)
	{
		if (i)
			ft_print_color(s);
		else
		{
			ft_putchar(' ');
			ft_print_color(s);
		}
	}
	else
	{
		if (i)
			ft_putstr(s);
		else
		{
			ft_putchar(' ');
			ft_putstr(s);
		}
	}
}
