/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rra.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 15:47:36 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:23:45 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_rra(t_env *env)
{
	t_ps	*tmp;

	if (env->a_last != NULL && env->a_last->prev != NULL && \
			env->a_last->prev->prev != NULL)
	{
		tmp = env->a_last;
		tmp->next = env->a_first;
		env->a_first->prev = tmp;
		env->a_first = tmp;
		tmp = env->a_last->prev;
		env->a_first->prev = NULL;
		env->a_last = tmp;
		env->a_last->next = NULL;
		if (!env->rrr)
			ft_operation(env, "rra");
		if (env->v)
			ft_option(env, 'v');
	}
	else
		ft_sa(env);
	return (1);
}
