/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_double.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/13 18:13:43 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 16:32:16 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_check_bis(t_env *env, t_ps *tmp)
{
	if (env->c_first->nbr > tmp->nbr)
		ft_ps_add(&env->c_first, &env->c_last, tmp);
	else if (env->c_first->nbr == tmp->nbr)
		ft_error_gestion("Error\n");
}

void		ft_check_double(t_env *env, t_ps *tmp)
{
	t_ps *cpy;

	if (env->c_first == NULL)
		ft_ps_add(&env->c_first, &env->c_last, tmp);
	else if (env->c_first->nbr < tmp->nbr)
	{
		cpy = env->c_first;
		while (cpy->nbr < tmp->nbr && cpy->next != NULL)
			cpy = cpy->next;
		if (cpy->next == NULL)
		{
			if (cpy->nbr < tmp->nbr)
				ft_ps_add_end(&env->c_first, &env->c_last, tmp);
			else if (cpy->nbr == tmp->nbr)
				ft_error_gestion("Error\n");
			else if (cpy->nbr > tmp->nbr)
				ft_ps_add_btw(&cpy->prev, &cpy, tmp);
		}
		else if (cpy->nbr == tmp->nbr)
			ft_error_gestion("Error\n");
		else
			ft_ps_add_btw(&cpy->prev, &cpy, tmp);
	}
	else if ((env->c_first->nbr > tmp->nbr) || (env->c_first->nbr == tmp->nbr))
		ft_check_bis(env, tmp);
}
