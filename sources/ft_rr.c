/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rr.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/11 16:46:47 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/17 17:03:06 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_rr(t_env *env)
{
	env->rr = 1;
	ft_ra(env);
	ft_rb(env);
	env->rr = 0;
	ft_operation(env, "rr");
	if (env->v)
		ft_option(env, 'v');
	return (1);
}
