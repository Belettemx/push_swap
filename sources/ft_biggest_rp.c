/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_biggest_rp.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/17 18:58:12 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 14:20:16 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_biggest_rp(t_ps *first)
{
	int		rp;
	t_ps	*a;

	a = first;
	rp = 0;
	while (a->next != NULL)
	{
		if (a->rp > rp)
			rp = a->rp;
		a = a->next;
	}
	if (a->rp > rp)
		rp = a->rp;
	return (rp);
}
