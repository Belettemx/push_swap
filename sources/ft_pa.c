/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pa.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/10 17:49:31 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 16:27:36 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ft_pa_ter(t_env *env, int i)
{
	t_ps *tmp;

	tmp = env->b_first;
	if (i)
	{
		env->b_first = env->b_first->next;
		env->b_first->prev = NULL;
	}
	else
	{
		env->b_first = NULL;
		env->b_last = NULL;
	}
	tmp->prev = NULL;
	env->a_first->prev = tmp;
	tmp->next = env->a_first;
	env->a_first = tmp;
}

static void		ft_pa_bis(t_env *env, int i)
{
	t_ps *tmp;

	tmp = env->b_first;
	env->a_first = tmp;
	env->a_last = tmp;
	if (i)
	{
		env->b_first = env->b_first->next;
		env->b_first->prev = NULL;
	}
	else
	{
		env->b_first = NULL;
		env->b_last = NULL;
	}
	env->a_first->next = NULL;
	env->a_first->prev = NULL;
}

int				ft_pa(t_env *env)
{
	int		i;

	if (env->b_first != NULL)
	{
		i = env->b_first->next != NULL ? 1 : 0;
		{
			if (env->a_first == NULL)
				ft_pa_bis(env, i);
			else if (env->a_first != NULL)
				ft_pa_ter(env, i);
		}
		ft_operation(env, "pa");
		if (env->v)
			ft_option(env, 'v');
	}
	return (1);
}
