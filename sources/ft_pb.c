/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pb.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/10 17:49:31 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/18 16:27:37 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		ft_pb_ter(t_env *env, int i)
{
	t_ps *tmp;

	tmp = env->a_first;
	if (i)
	{
		env->a_first = env->a_first->next;
		env->a_first->prev = NULL;
	}
	else
	{
		env->a_first = NULL;
		env->a_last = NULL;
	}
	tmp->prev = NULL;
	env->b_first->prev = tmp;
	tmp->next = env->b_first;
	env->b_first = tmp;
}

static void		ft_pb_bis(t_env *env, int i)
{
	t_ps *tmp;

	tmp = env->a_first;
	env->b_first = tmp;
	env->b_last = tmp;
	if (i)
	{
		env->a_first = env->a_first->next;
		env->a_first->prev = NULL;
	}
	else
	{
		env->a_first = NULL;
		env->a_last = NULL;
	}
	env->b_first->next = NULL;
	env->b_first->prev = NULL;
}

int				ft_pb(t_env *env)
{
	int		i;

	if (env->a_first != NULL)
	{
		i = env->a_first->next != NULL ? 1 : 0;
		{
			if (env->b_first == NULL)
				ft_pb_bis(env, i);
			else if (env->b_first != NULL)
				ft_pb_ter(env, i);
		}
		ft_operation(env, "pb");
		if (env->v)
			ft_option(env, 'v');
	}
	return (1);
}
