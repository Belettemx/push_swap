/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_top_a_smallest_rp.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/16 13:39:58 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/31 22:39:20 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int ft_top_a_smallest_rp(t_env *env)
{
	int i;
	int b;

	i = 0;
	b = ft_r_or_rr(env->a_first, 0, 1) == 1 ? 1 : 2;
	if (b == 1)
	{
		while (env->a_first->rp != 1)
			i += ft_ra(env);
	}
	else
	{
		while (env->a_first->rp != 1)
			i += ft_rra(env);
	}
	return (i);
}
