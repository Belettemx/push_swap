# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/22 17:53:00 by agauci-d          #+#    #+#              #
#    Updated: 2015/04/01 21:34:55 by agauci-d         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap

CC = gcc

SOURCE = ./sources/ft_check_error.c ./sources/ft_error_gestion.c \
		 ./sources/ft_init_env.c ./sources/ft_ps_add.c ./sources/ft_ps_new.c \
		 ./sources/ft_ps_add_end.c ./sources/ft_ps_add_btw.c \
		  main.c ./sources/ft_sa.c ./sources/ft_check_double.c\
		 ./sources/ft_print_list.c ./sources/ft_sb.c ./sources/ft_pa.c \
		 ./sources/ft_pb.c ./sources/ft_ss.c ./sources/ft_ra.c \
		 ./sources/ft_rb.c ./sources/ft_rr.c ./sources/ft_rrb.c\
		 ./sources/ft_rra.c ./sources/ft_rrr.c \
		 ./sources/ft_algorithme.c ./sources/ft_simple_algo.c \
		 ./sources/ft_add_rp.c ./sources/ft_check_al_sort.c \
		 ./sources/ft_top_a_smallest_rp.c ./sources/ft_r_or_rr.c \
		 ./sources/ft_lstlen.c ./sources/ft_option.c ./sources/ft_operation.c \
		 ./sources/ft_diff_nbr.c ./sources/ft_two. ./sources/ft_biggest_rp.c\
		 ./sources/ft_mix_ab.c ./sources/ft_short_list.c \
		 ./sources/ft_check_sort_rest.c ./sources/ft_check_last_inverse.c

NOM = $(basename $(SOURCE))

OBJET = $(addsuffix .o, $(NOM))

FLAGS = -Wall -Werror -Wextra -I ./includes -I ./libft/includes -g3 -g

HEADER = push_swap.h

all: $(NAME)

$(NAME): $(OBJET)
	@make -C libft/
	@gcc -o $@ $(OBJET) $(FLAGS) -L libft/ -lft
	@echo "make push_swap OK"
	@echo "The options available for push_swap are :"
	@echo "      -v (print the state of both pile after each operation)"
	@echo "      -e (print the state of both pile only at the end)"
	@echo "      -c (print the number of operations needed to sort the list)"
	@echo "      -p (print the element position at the beginning and where it should be at the end)"
	@echo "      -r (print each type of operation with a different color)"

%.o: %.c
	@$(CC) -o $@ -c $< $(FLAGS)

clean:
	@make -C ./libft/ clean
	@/bin/rm -f $(OBJET)
	@echo "clean push_swap OK"

fclean: clean
	@make -C ./libft/ fclean
	@/bin/rm -f $(NAME)
	@echo "fclean push_swap OK"

re: fclean all
	@echo "re push_swap OK"

small_coffee :
	@echo "   )  "
	@echo "  (   "
	@echo " [_]) "

coffee :
	@echo "    (     )    ( )     ( "
	@echo "     )   (    )   (    ) "
	@echo "      (  )   )     )  ( "
	@echo "        _____________ "
	@echo "       <_____________> ___"
	@echo "       |             |/ _ \ "
	@echo "       |               | | | "
	@echo "       |               |_| | "
	@echo "    ___|             |\___/  "
	@echo "   /    \___________/    \ "
	@echo "   \_____________________/ "


.PHONY: all clean fclean re coffee small_coffee
