/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 13:46:52 by agauci-d          #+#    #+#             */
/*   Updated: 2015/03/31 21:10:06 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		ft_find_opt(char **av, t_env *env)
{
	int cpt;

	cpt = 1;
	while (av[cpt])
	{
		if (ft_strcmp("-v", av[cpt]) == 0)
			env->v = 1;
		else if (ft_strcmp("-c", av[cpt]) == 0)
			env->c = 1;
		else if (ft_strcmp("-e", av[cpt]) == 0)
			env->e = 1;
		else if (ft_strcmp("-p", av[cpt]) == 0)
			env->p = 1;
		else if (ft_strcmp("-r", av[cpt]) == 0)
			env->r = 1;
		else
			break ;
		cpt++;
	}
	if (av[cpt] == NULL)
		ft_error_gestion("Error\n");
	if (cpt > 1)
		return (cpt - 1);
	else
		return (0);
}

static void		ft_params_to_list(int i, char **av, t_env *env)
{
	int		b;
	int		ret;
	t_ps	*tmp;
	t_ps	*tmp2;

	ret = ft_find_opt(av, env);
	while (i > ret)
	{
		b = ft_check_error(av[i]);
		tmp = ft_ps_new(b, (i - ret), 1);
		tmp2 = ft_ps_new(b, (i - ret), 1);
		ft_ps_add(&env->a_first, &env->a_last, tmp);
		ft_check_double(env, tmp2);
		i--;
		env->isa_empty++;
		env->isc_empty++;
	}
	ft_add_rp(env);
}

static void		ft_print_else(int cpt, t_env *env)
{
	if (cpt > 0)
		ft_putchar('\n');
	if (env->e || env->v)
		ft_option(env, 'e');
	if (env->c)
	{
		ft_putstr("operations number = ");
		ft_putnbr(cpt);
		ft_putchar('\n');
	}
}

int				main(int ac, char **argv)
{
	int		i;
	int		sort;
	t_env	env;
	int		cpt;

	cpt = 0;
	if (ac > 1)
	{
		i = ac - 1;
		ft_init_env(&env);
		ft_params_to_list(i, argv, &env);
		sort = ft_check_al_sort(&env);
		if (sort == 0 && (env.diff = ft_diff_nbr(&env)) > 10)
			cpt = ft_algorithme(&env);
		else if (sort == 0 && (env.diff = ft_diff_nbr(&env)) > 2)
			cpt = ft_short_list(&env);
		else if (sort == 0 && ft_diff_nbr(&env) > 0 && ft_diff_nbr(&env) \
				< 3)
			cpt = ft_simple_algo(&env);
		ft_print_else(cpt, &env);
	}
	return (0);
}
